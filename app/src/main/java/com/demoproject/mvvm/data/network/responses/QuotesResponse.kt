package com.demoproject.mvvm.data.network.responses

import net.demoproject.mvvmsampleapp.data.db.entities.Quote

data class QuotesResponse (
    val isSuccessful: Boolean,
    val quotes: List<Quote>
)