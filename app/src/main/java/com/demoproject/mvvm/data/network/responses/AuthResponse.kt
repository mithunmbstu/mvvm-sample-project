package com.demoproject.mvvm.data.network.responses

import net.demoproject.mvvmsampleapp.data.db.entities.User

data class AuthResponse(
    val isSuccessful : Boolean?,
    val message: String?,
    val user: User?
)