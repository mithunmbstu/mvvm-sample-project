package com.demoproject.mvvm.ui.home.quotes

import androidx.lifecycle.ViewModel;
import net.demoproject.mvvmsampleapp.data.repositories.QuotesRepository
import net.demoproject.mvvmsampleapp.util.lazyDeferred

class QuotesViewModel(
    repository: QuotesRepository
) : ViewModel() {

    val quotes by lazyDeferred {
        repository.getQuotes()
    }
}
