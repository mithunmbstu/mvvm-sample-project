package com.demoproject.mvvm.ui.home.profile

import androidx.lifecycle.ViewModel;
import net.demoproject.mvvmsampleapp.data.repositories.UserRepository

class ProfileViewModel(
    repository: UserRepository
) : ViewModel() {

    val user = repository.getUser()

}
