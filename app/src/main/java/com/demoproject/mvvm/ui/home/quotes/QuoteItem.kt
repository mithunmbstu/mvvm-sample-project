package com.demoproject.mvvm.ui.home.quotes

import com.xwray.groupie.databinding.BindableItem
import net.demoproject.mvvmsampleapp.R
import net.demoproject.mvvmsampleapp.data.db.entities.Quote
import net.demoproject.mvvmsampleapp.databinding.ItemQuoteBinding

class QuoteItem(
    private val quote: Quote
) : BindableItem<ItemQuoteBinding>(){

    override fun getLayout() = R.layout.item_quote

    override fun bind(viewBinding: ItemQuoteBinding, position: Int) {
        viewBinding.setQuote(quote)
    }
}